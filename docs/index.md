# 딥러닝 개념 개요 <sup>[1](#footnote_1)</sup>

> <font size="3">딥 러닝이 무엇이고 어떻게 작동하는지 알아본다.</font>

## 목차

1. [딥 러닝이란?](./intro.md#sec_01)
1. [딥 러닝은 어떻게 작동할까?](./intro.md#sec_02)
1. [딥 러닝의 이점과 과제](./intro.md#sec_03)
1. [딥 러닝의 어플리케이션](./intro.md#sec_04)
1. [딥 러닝을 시작하는 방법](./intro.md#sec_05)

<a name="footnote_1">1</a>: [DL Tutorial 1 — Introduction to Deep Learning Concepts](https://medium.com/ai-advances/dl-tutorial-1-introduction-to-deep-learning-concepts-77cbe84cfaf9?sk=e5dd5edbf7e09d5747fa06b78441004a)를 편역하였습니다.
